import { Component } from '@angular/core';
import { Player } from './interfaces/player.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'pig-dice-game';

  diceScore: number = 1;
  roundScore: number = 0;
  players: Player[] = [
    { name: 'Player 1', score: 0 },
    { name: 'Player 2', score: 0 },
  ];
  currentPlayer: string = 'Player 1';
  isHavingPlayerWin: boolean = false;

  togglePlayer() {
    this.currentPlayer =
      this.currentPlayer === 'Player 1' ? 'Player 2' : 'Player 1';
    this.diceScore = 1;
    this.roundScore = 0;
  }

  handleClickRoll() {
    const random = Math.floor(Math.random() * 6) + 1;
    this.diceScore = random;
    if (random === 1) {
      this.togglePlayer();
    } else {
      const { currentPlayer, players } = this;
      const newScore = this.roundScore + random;
      const newPlayers = [...players];
      const idx = players.findIndex((item) => item.name === currentPlayer);
      const playerScore =
        players.find((item) => item.name === currentPlayer)?.score || 0;
      this.roundScore = newScore;
      if (playerScore + newScore >= 100) {
        const { name, score } = newPlayers[idx];
        newPlayers[idx] = { name, score: score + newScore };
        this.players = newPlayers;
        this.isHavingPlayerWin = true;
        alert(`${currentPlayer} win!!`);
      }
    }
  }

  handleClickKeep() {
    const { currentPlayer, players, roundScore } = this;
    const newPlayers = [...players];
    const idx = players.findIndex((item) => item.name === currentPlayer);
    const { name, score } = newPlayers[idx];
    newPlayers[idx] = { name, score: score + roundScore };
    this.players = newPlayers;
    this.togglePlayer();
  }

  handleClickNewGame() {
    this.currentPlayer = 'Player 1';
    this.diceScore = 1;
    this.roundScore = 0;
    this.players = [
      { name: 'Player 1', score: 0 },
      { name: 'Player 2', score: 0 },
    ];
    this.isHavingPlayerWin = false;
  }
}
