import { Component, Input, OnInit } from '@angular/core';
import { Player } from 'src/app/interfaces/player.interface';

@Component({
  selector: 'app-player-score',
  templateUrl: './player-score.component.html',
  styleUrls: ['./player-score.component.css'],
})
export class PlayerScoreComponent implements OnInit {
  @Input() player: Player = { name: '', score: 0 };
  @Input() currentPlayer: string = '';

  constructor() {}

  ngOnInit(): void {}
}
