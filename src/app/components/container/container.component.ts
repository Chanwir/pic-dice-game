import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { Player } from 'src/app/interfaces/player.interface';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css'],
})
export class ContainerComponent implements OnInit {
  app: AppComponent;

  constructor(private appComponent: AppComponent) {
    this.app = appComponent;
  }

  ngOnInit(): void {}
}
