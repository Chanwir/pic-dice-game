import { Component, OnInit, ViewChild } from '@angular/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-dice-score',
  templateUrl: './dice-score.component.html',
  styleUrls: ['./dice-score.component.css'],
})
export class DiceScoreComponent implements OnInit {
  app: AppComponent;
  constructor(private appComponent: AppComponent) {
    this.app = appComponent;
  }

  ngOnInit(): void {}
}
