import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiceScoreComponent } from './dice-score.component';

describe('DiceScoreComponent', () => {
  let component: DiceScoreComponent;
  let fixture: ComponentFixture<DiceScoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiceScoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiceScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
